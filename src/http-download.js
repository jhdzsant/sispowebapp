import axios from "axios";

export default axios.create({
  //baseURL: "https://localhost:44307/api",
  baseURL: "https://localhost:5001/api",
  //baseURL: "http://sispoapi-test.us-east-1.elasticbeanstalk.com/api",
  headers: {
    "accept": "*/*",
  }
});