//import DashboardLayout from "@/pages/Dashboard/Layout/DashboardLayout.vue";
import DashboardLayout from "@/paginas/Layout/dashboard.vue";
import AuthLayout from "@/pages/Dashboard/Pages/AuthLayout.vue";
import ejemplo from "@/pages/Dashboard/Forms/ExtendedForms.vue";

// Dashboard pages
import DashboardAdmin from "@/paginas/Dashboard/admin.vue";
import DashboardRepresentacion from "@/paginas/Dashboard/representante.vue";
import DashboardGenerico from "@/paginas/Dashboard/generico.vue";

// Pages
import Login from "@/pages/Dashboard/Pages/Login.vue";

//General
import listadoPolizas from "@/paginas/General/Polizas/listado.vue";
import tabsPoliza from "@/paginas/General/Polizas/tabs.vue";

//Administracion
import cxp from "@/paginas/Administracion/CuentasXPagar/crud.vue";
import CuentasBancarias from "@/paginas/Administracion/CuentasBancarias/crud.vue";
import categorias from "@/paginas/Administracion/Categorias/crud.vue";

//Seguridad
import usuarios from "@/paginas/Administracion/seguridad/UsuariosCrud.vue";

// Solicitudes
import solicitud from "@/paginas/Solicitudes/Solicitudes.vue";
import nueva_solicitud from "@/paginas/Solicitudes/NuevaSolicitud.vue";


//Catalogos
import areas from "@/paginas/Catalogos/areas.vue";
import parametros from "@/paginas/Administracion/Parametros/crud.vue";
import centrocostos from "@/paginas/Catalogos/centrocostos.vue";
import tipodocumento from "@/paginas/Catalogos/tipodocumento.vue";
import estados from "@/paginas/Catalogos/estados.vue"
import tipoparentesco from "@/paginas/Catalogos/tipoparentesco.vue"
import tipoinmobiliario from "@/paginas/Catalogos/tipoinmobiliario.vue"
import tipoproceso from "@/paginas/Catalogos/tipoproceso.vue";
import tiporefPersonales from "@/paginas/Catalogos/tiporefpersonal.vue";



//Módulo Contable
import movimientos from "@/paginas/Administracion/Movimientos/listado.vue";
import movimientoscrud from "@/paginas/Administracion/Movimientos/crud.vue";
// import ejemplo from "@/pages/Dashboard/Tables/PaginatedTables.vue";

//Representaciones
import crudEventos from "@/paginas/Representaciones/eventos.vue";
import representaciones from "@/paginas/Catalogos/representaciones.vue";
import crudlistasnegras from "@/paginas/Representaciones/listasnegras.vue";
import crudclienteusuarios from "@/paginas/Representaciones/clienteusuarios.vue";

//Reportes 
import reporteventasgerencia from "@/paginas/Reportes/ventas.vue";

//Administración
import ConfCuentaRep from "@/paginas/Administracion/confcuentasrep/crud.vue";

// Calendar
import Calendar from "@/pages/Dashboard/Calendar.vue";

//Settings User
import User from "@/paginas/User/UserProfile.vue";

//Soluciones
import solucionesIndex from "@/paginas/Soluciones/Index.vue";
import tabssoluciones from "@/paginas/Soluciones/tabs.vue";

//No found 
import Nofound from "@/paginas/Nofound.vue";

let authPages = {
    path: "/",
    component: AuthLayout,
    name: "Authentication",
    children: [{
        path: "/login",
        name: "Login",
        component: Login
    }]
};

let authsolucionesPages = {
    path: "/",
    component: DashboardLayout,
    name: "Soluciones",
    children: [{
        path: "/soluciones",
        name: "soluciones",
        component: solucionesIndex
    },{
        path: "/tabssoluciones",
        name: "tabssoluciones",
        component: tabssoluciones
    }]
};

let administracionpages = {
    path: "/",
    component: DashboardLayout,
    name: "administacion",
    children: [
        {
            path: "/ConfCuentaRep",
            name: "ConfCuentaRepr",
            component: ConfCuentaRep
        }, {
            path: "/cuentasxpagars",
            name: "CuentasXpagar",
            component: cxp
        },
        {
            path: "/movimientos",
            name: "Movimientos",
            component: movimientos
        },
        {
            path: "/movimientos/crud/:id",
            name: "movimientoscrud",
            component: movimientoscrud
        },
        {
            path: "/parametros",
            name: "parametros",
            component: parametros
        },
        {
            path: "/CuentasBancarias",
            name: "CuentasBancarias",
            component: CuentasBancarias
        },
        {
            path: "/categoriaes",
            name: "categoriaes",
            component: categorias
        }]
};

let representacionespages = {
    path: "/",
    component: DashboardLayout,
    name: "representaciones",
    children: [{
        path: "/eventos",
        name: "eventos",
        component: crudEventos
    }, 
    {
        path: "/listanegras",
        name: "listasnegras",
        component: crudlistasnegras
    },
    {
        path: "/clienteusuarios",
        name: "clienteusuarios",
        component: crudclienteusuarios
    },
    {
        path: "calendarios",
        name: "Calendar",
        components: { default: Calendar }
    },]
};

let reportes = {
    path: "/",
    component: DashboardLayout,
    name: "reportes",
    children: [{
        path: "/reportes",
        name: "reporteventasgerencia",
        component: reporteventasgerencia
    }]
};

let catalogos = {
    path: "/",
    component: DashboardLayout,
    name: "catalogos",
    children: [ {
        path: "TipoProcesoes",
        name: "TipoProcesoes",
        component:  tipoproceso 
    },{
        path: "/tipoinmobiliarios",
        name: "tipoinmobiliario",
        component:  tipoinmobiliario 
    },
    {
        path: "/tipoparentescoes",
        name: "tipoparentesco",
        component:  tipoparentesco 
    },
    {
        path: "/TipoRefPersonals",
        name: "TipoRefPersonals",
        component:  tiporefPersonales 
    }
]
};

const routes = [{
    path: "/",
    redirect: "/login",
    name: "login"
},
    authPages,
    administracionpages,
    representacionespages,
    reportes,
    catalogos,
    authsolucionesPages,
{
    path: "/",
    component: DashboardLayout,
    children: [{
        path: "admin",
        name: "admin",
        components: { default: DashboardAdmin }
    },
    {
        path: "representate",
        name: "representate",
        components: { default: DashboardRepresentacion }
    },
    {
        path: "generico",
        name: "generico",
        components: { default: DashboardGenerico }
    },
    {
        path: "polizas",
        name: "polizas",
        components: { default: listadoPolizas }
    },
    {
        path: "tabspoliza",
        name: "tabspoliza",
        components: { default: tabsPoliza }
    },
    {
        path: "usuarios",
        name: "usuarios",
        components: { default: usuarios }
    },
    {
        path: "areas",
        name: "areas",
        components: { default: areas }
    },
    {
        path: "centrocostos",
        name: "centrocostos",
        components: { default: centrocostos }
    },
    {
        path: "representacions",
        name: "representaciones",
        components: { default: representaciones }
    },
    {
        path: "ejemplo",
        name: "ejemplo",
        components: { default: ejemplo }
    },
    {

        path: "/",
        component: DashboardLayout,
        children: [{
                path: "admin",
                name: "admin",
                components: { default: DashboardAdmin }
            },
            {
                path: "dashboard",
                name: "Dashboard",
                components: { default: DashboardAdmin }
            },
            {
                path: "polizas",
                name: "polizas",
                components: { default: listadoPolizas }
            },
            {
                path: "tabspoliza",
                name: "tabspoliza",
                components: { default: tabsPoliza }
            },
            {
                path: "usuarios",
                name: "usuarios",
                components: { default: usuarios }
            },
            {
                path: "areas",
                name: "areas",
                components: { default: areas }
            },
            {
                path: "tipoinmobiliarios",
                name: "tipoinmobiliario",
                components: { default: tipoinmobiliario }
            },
            {
                path: "centrocostos",
                name: "centrocostos",
                components: { default: centrocostos }
            },
            {
                path: "tipodocumentoes",
                name: "tipodocumento",
                components: { default: tipodocumento }
            },
            {
                path: "estados",
                name: "estados",
                components: { default: estados }
            },
            {
                path: "tipoparentescoes",
                name: "tipoparentesco",
                components: { default: tipoparentesco }
            },
            {
                path: "representacions",
                name: "representaciones",
                components: { default: representaciones }
            },           
            {
                path: "ejemplo",
                name: "ejemplo",
                components: { default: ejemplo }
            },

            // {
            //     path: "solicituds",
            //     name: "Solicitudes",
            //     components: { default: solicitud }
            // },
            // {
            //     path: "nuevasolicitud",
            //     name: "NuevaSolicitud",
            //     components: { default: nueva_solicitud }
            // },
        ]
    },
    {

        path: "solicituds",
        name: "Solicitudes",
        components: { default: solicitud }
    },
    {
        path: "nuevasolicitud",
        name: "NuevaSolicitud",
        components: { default: nueva_solicitud }
    },
    {
        path: "User",
        name: "User",
        components: { default: User }
    },
    
    ]
}
];
export default routes;