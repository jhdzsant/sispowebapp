import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from 'vuex-persistedstate';
import * as Cookies from 'js-cookie';

Vue.use(Vuex);

const store = new Vuex.Store({  
  
  state: {
    token: null,
    General: {
      Poliza: 0,
      Solicitud: 0,
      solucionId: 0
    },
    usuario: {
      activo: null,
      areaId: null,
      contraEncrypt: null,
      isResponsanle: null,
      representacionId: null,
      usuarioApellidoMaterno: null,
      usuarioApellidoPaterno: null,
      usuarioCelular: null,
      usuarioInmobiliaria: null,
      usuarioNomCompleto: null,
      usuarioNombre: null,
      usuarioPadreId: null,
      usuarioTelefono: null,
      usuariosId: null,
      usurioEmail: null,
      imagen: null,
      dashboard: null
    },
    cantidad: {
      soluciones: 0,
      polizas: 0,
      solicitudes: 0
    },
    Movimientos: {
      DetallePolizas: [],
      Transferencias: [],
      CuentasXPagar: [],
      CuentasXCobrar: []
    },
    Cuenta: {
      Id: 0,
      Nombre: null,
      Saldo: 0,
      entrada: 0,
      salida: 0,
      total: 0,
    },
  },
  plugins: [createPersistedState()],//Nos sirve para que no se pierdan los datos  
});

export default store;