import http from "../../http-commons";

class TipoProcesoPo {
  all() {
    return http.get('/TipoProcesoPo');
  }
  area(areaId) {
    return http.get(`/TipoProcesoPo/area/${areaId}`);
  }
  post(data) {
    return http.post('/TipoProcesoPo',data);
  }
  put(id,data) {
    return http.put(`/TipoProcesoPo/${id}`,data);
  }
  delete(id) {
    return http.delete(`/TipoProcesoPo/${id}`);
  }
}

export default new TipoProcesoPo();