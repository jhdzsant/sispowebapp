import http from "../../http-commons";

class cuentasbancarias {
  all() {
    return http.get('/CuentasBancarias');
  }
  post(data) {
    return http.post('/CuentasBancarias',data);
  }
  put(id,data) {
    return http.put(`/CuentasBancarias/${id}`,data);
  }
  delete(id) {
    return http.delete(`/CuentasBancarias/${id}`);
  }
  Usuario(id){
    return http.get(`/CuentasBancarias/Usuario/${id}`);
  }
}

export default new cuentasbancarias();