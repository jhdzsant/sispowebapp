import http from "../../http-commons";

class solucion {
  all() {
    return http.get('/solucion');
  }
  get(id) {
    return http.get(`/solucion/Representacion/${id}`);
  }
  post(solucion) {
    return http.post('/solucion', solucion);
  }
  put(id, solucion) {
    return http.put(`/solucion/${id}`, solucion);
  }
  delete(id) {
    return http.delete(`/solucion/${id}`);
  }
}

export default new solucion();