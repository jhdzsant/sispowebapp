import http from "../../http-commons";

class eventos {
  getVentasGerencia(FechaInicio,FechaFin,FechaInicioPoliza,FechaFinPoliza,IdSolicitudInicio,IdSolicitudFin,IdPolizaInicio,IdPolizafin,RepresentacionId,TipoProcesoPoId,TipoProcesoId,UsuarioId) {
    //console.log(FechaInicio,FechaFin,FechaInicioPoliza,FechaFinPoliza,IdSolicitudInicio,IdSolicitudFin,IdPolizaInicio,IdPolizafin);
    return http.get(`/Reportes/${FechaInicio}/${FechaFin}/${FechaInicioPoliza}/${FechaFinPoliza}/${IdSolicitudInicio}/${IdSolicitudFin}/${IdPolizaInicio}/${IdPolizafin}/${RepresentacionId}/${TipoProcesoPoId}/${TipoProcesoId}/${UsuarioId}`);
  }
}

export default new eventos();