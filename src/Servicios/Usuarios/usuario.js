import http from "../../http-commons";

class usuario {
  all(id) {
    return http.get(`/usuario/${id}`);
  }
  post(usuario) {
    return http.post('/usuario',usuario);
  }
  put(id, usuario) {
    return http.put(`/usuario/${id}`,usuario);
  }
  delete(id) {
    return http.delete(`/usuario/${id}`);
  }
  getUser(id) {
    return http.get(`/usuario/user/${id}`);
  }
}

export default new usuario();