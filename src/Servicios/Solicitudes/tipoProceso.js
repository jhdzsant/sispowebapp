import http from "../../http-commons";

class TipoProceso {
    tiposProceso() {
        return http.get('/TipoProceso');
    }
}

export default new TipoProceso();