import http from "../../http-commons";

class Solicitudes {
    getSolicitudes(tipoProceso, fechaInicio, fechafin, renovacion, usuario) {
        return http.get(`/Solicitud/${tipoProceso}/${fechaInicio}/${fechafin}/${renovacion}/${usuario}`);
    }
    BuscaSolicitud(id,usuarioid){
        return http.get(`/Solicitud/solicitudespecifica/${id}/${usuarioid}`);
      }
}

export default new Solicitudes();