import http from "../../http-commons";

class Estados {
    getEstados() {
        return http.get('/Estado');
    }
}

export default new Estados();