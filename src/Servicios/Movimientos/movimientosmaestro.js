import http from "../../http-commons";

class movimientomaestro {
  get(id,userId) {
    return http.get(`/MovimientoMaestro/${id}/${userId}`);
  }
}

export default new movimientomaestro();