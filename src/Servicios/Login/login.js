import http from "../../http-commons";
import SecurityService from "../../Auth/auth2";
class Login {
  
   login(usuario) {     
    SecurityService.getToken();
    return http.post('/Login', usuario);
  }
}

export default new Login();