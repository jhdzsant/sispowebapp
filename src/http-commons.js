import axios from "axios";
import store from "./Store/index";

const storeToken = store.state.token;
//V1
export default axios.create({
    //baseURL: "https://localhost:44307/api",
    baseURL: "https://localhost:5001/api",
    //baseURL: "https://sispo.website/api",
    //baseURL: "http://sispoapi-test.us-east-1.elasticbeanstalk.com/api",
    headers: {
        "Content-type": "application/json-patch+json",
        "accept": "text/plain",
        "Authorization": storeToken
    }
});
